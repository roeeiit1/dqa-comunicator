import 'fontsource-roboto/300.css';
import React, { useState } from 'react';
import { DatePicker, MuiPickersUtilsProvider } from '@material-ui/pickers';
import { FormControl, InputLabel, Select, MenuItem, Button, Typography } from '@material-ui/core'
import DayjsUtil from '@date-io/dayjs';
import './App.css';

function App() {
  const defaultDate = new Date();
  defaultDate.setMinutes(0);
  defaultDate.setHours(0);
  defaultDate.setMilliseconds(0);
  defaultDate.setSeconds(0);
  const [selectedMinDate, handleMinDateChange] = useState(defaultDate);
  const maxDefualtDate = new Date();
  maxDefualtDate.setDate(defaultDate.getDate() + 1);
  const [selectedMaxDate, handleMaxDateChange] = useState(maxDefualtDate);
  const [alias, handleAliasChange] = useState();

  const handleFormSend = () => {
    if (alias != null && selectedMaxDate != null && selectedMinDate != null) {
      console.log(alias.target.value, selectedMaxDate.toISOString(), selectedMinDate.toISOString());
    }
    else {
      console.error(alias, selectedMaxDate, selectedMinDate);
    }
  }
  return (
    <div className="main">
      <div className="textDiv">
        <Typography variant="h2" align="center" color="primary">
          DQA Creator
        </Typography>
      </div>
      <div className="flexTitle">
        <Typography variant="h4" color="primary">
          Min Date
        </Typography>
        <Typography variant="h4" color="primary">
          Max Date
        </Typography>
      </div>
      <div className="flexDate">
        <MuiPickersUtilsProvider utils={DayjsUtil}>
          <DatePicker
            orientation="landscape"
            variant="inline"
            ampm={false}
            value={selectedMinDate}
            onChange={handleMinDateChange}
            format="YYYY/MM/DD"
            showTodayButton={true}
            autoOk={true}
            maxDate={selectedMaxDate}
          />
          <DatePicker
            orientation="landscape"
            variant="inline"
            ampm={false}
            value={selectedMaxDate}
            onChange={handleMaxDateChange}
            format="YYYY/MM/DD"
            showTodayButton={true}
            autoOk={true}
            minDate={selectedMinDate}
            maxDate={maxDefualtDate}
          />
        </MuiPickersUtilsProvider>
      </div>
      <div className="formDiv">
        <FormControl fullWidth variant="outlined">
          <InputLabel>DQA Index Alias</InputLabel>
          <Select
            value={alias}
            onChange={handleAliasChange}
            required
          >
            <MenuItem value={"Kupot"}>Kupot</MenuItem>
          </Select>
        </FormControl>
      </div>
      <div className="buttonDiv">
        <Button variant="contained" color="primary" onClick={handleFormSend}>
          Generate Sample
        </Button>
        <Button variant="contained" color="primary" onClick={handleFormSend}>
          Download Items
        </Button>
      </div>
    </div>
  );
}

export default App;
